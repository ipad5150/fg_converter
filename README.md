# FG Converter

## Converts Fantasy Grounds campaigns into FoundryVTT worlds.

### **Current Generic Support Status:** 
(This works for all game systems)


- ✅ Refence Manuals and Books display
- ✅ Scenes including walls and doors (even for FGC)
- ✅ Journals
- ✅ Journals on Scenes (map pins)
- ✅ Rollable tables
- ✅ Image asset de-noise and upscaling [Token Example](https://i.imgur.com/2OdmEXO.png), [Map Example](https://i.imgur.com/BkyevwC.png) (FG has "optimized" image assets that look really bad in modern VTTs) 
- ✅ Token placement on scenes

![](examples.webm)

---
### **System Specific Support:**

#### **5e**

| Character | NPC | Spells | Actions | Items | Traits | Features | Languages | Inventory |
| ------ | ------ |------ |------ |------ |------ |------ |------ |------  |
| ✓  | ✓  |✓  |✓  |✓  |✓  |✓  |✓  |✓  |

![](5e.e.webm)

#### **pf2e**
https://gitlab.com/jesusafier/fg_converter/-/wikis/Community-System-Support/pf2e

#### **swade**
https://gitlab.com/jesusafier/fg_converter/-/wikis/Community-System-Support/swade

#### **coc**
https://gitlab.com/jesusafier/fg_converter/-/wikis/Community-System-Support/coc

---
### Links
Please see the [wiki](https://gitlab.com/jesusafier/fg_converter/-/wikis/Community-System-Support) for mapping and conversion knowledge base.

Please see the [issues](https://gitlab.com/jesusafier/fg_converter/-/issues) for reporting bugs.

**Join the discussions on our [Discord](https://discord.gg/467HAfZ)**

---
### Getting the tool
Available for 5$/10$ on my Patreon https://www.patreon.com/foundry_grape_juice

![](https://i.imgur.com/1pL1XaQ.png)